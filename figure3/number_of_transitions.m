% overall figure size
aspectRatio = 4/3;
screenHeight = 600; % height in pixels for the window
vectorHeight = 20; % height in inches for exporting
resolution = 300; % DPI for exporting (higher -> larger image)

FontSize = 22;
LineWidth = 1.8;

shapes = {'tube', 'tip'};
colors = [26 128 184; ...
          256 128 0] / 256;

for s = 1:length(shapes)
    cellLayers{s} = readtable(['layers_' shapes{s} '.csv']);
    transitions{s} = readtable(['location_' shapes{s} '.csv']);
    nCells{s} = max(transitions{s}.cell);
    nnc{s} = zeros(nCells{s},1);
    for i = 1:nCells{s}
        nnc{s}(i) = sum(abs(transitions{s}.type(transitions{s}.cell == i)));
    end
end

figure
hold on
set(gcf, 'Position', [0 0 screenHeight*[aspectRatio 1]], 'Color', 'white')
set(gcf, 'PaperSize', vectorHeight*[aspectRatio 1]) % for PDF and postscript export
set(gcf, 'PaperPosition', [0 0 get(gcf, 'PaperSize')]) % for image export

nmin = min([min(nnc{1}) min(nnc{2})]);
nmax = max([max(nnc{1}) max(nnc{2})]);
x = (nmin:nmax)';
h = zeros(nmax-nmin+1,2);
for s = 1:length(shapes)
    [h(:,s), ~] = histcounts(nnc{s}, nmin-0.5:1:nmax+0.5);
end
b = bar(x+0.15*ones(size(x))*[-1 1], h./(ones(size(h,1),1)*sum(h,1)), 'BarWidth', 4.5);
for s = 1:length(shapes)
    set(b(s), 'FaceColor', colors(s,:), 'EdgeColor', 'none')
end

for s = 1:length(shapes)
    [lambda,lambdaCI] = mle(nnc{s}, 'distribution', 'Poisson');
    pd = fitdist(nnc{s}, 'Poisson');
    [~,p,st] = chi2gof(nnc{s}, 'CDF', pd);
    plot(x, pdf('Poisson', x, lambda), 'ko-', 'LineWidth', LineWidth, 'MarkerFaceColor', colors(s,:), 'MarkerSize', 10)
    err = mean(abs(lambdaCI-lambda));
    dispersion = var(nnc{s})/mean(nnc{s});
    text(6, 0.3, 'Null hypothesis: Poisson distribution', 'FontSize', FontSize);
    text(7.5, 0.085+0.085*(3-s), ['T' shapes{s}(2:end) ' :  N = ' num2str(nCells{s}) ' cells' newline '\chi^2 = ' num2str(round(st.chi2stat,2)) ', p = ' num2str(round(p,3)) newline 'Dispersion index \sigma^2/\mu = ' num2str(round(dispersion,3))], 'FontSize', FontSize, 'Color', colors(s,:), 'Interpreter', 'tex')
    text(7.5, 0.045+0.085*(3-s), ['Mean \mu = ' num2str(round(lambda,2)) '\pm' num2str(round(err,2)) ' (95% CI)'], 'FontSize', FontSize, 'Color', colors(s,:), 'Interpreter', 'tex')
end

xticks(x)
xlim([nmin-0.5 nmax+0.5])
set(gca, 'FontSize', FontSize, 'LineWidth', LineWidth, 'TickDir', 'out')
box('off')
xlabel('number of T1 transitions per cell')
ylabel('probability')
ytickformat('%.2f')

% print(gcf, 'transition_number_distribution.pdf', '-dpdf', ['-r' num2str(resolution)]);
% print(gcf, 'transition_number_distribution.png', '-dpng', ['-r' num2str(resolution)]);

figure
hold on
set(gcf, 'Position', [0 0 screenHeight*[aspectRatio 1]], 'Color', 'white')
set(gcf, 'PaperSize', vectorHeight*[aspectRatio 1]) % for PDF and postscript export
set(gcf, 'PaperPosition', [0 0 get(gcf, 'PaperSize')]) % for image export

edges = (0:0.1:1)';
x = (edges(1:end-1) + edges(2:end)) / 2;
h = zeros(length(x),2);
for s = 1:length(shapes)
    positions{s} = zeros(size(transitions{s}.layer));
    for i = 1:length(transitions{s}.layer)
        cell = transitions{s}.cell(i);
        minLayer = cellLayers{s}.minLayer(cell);
        maxLayer = cellLayers{s}.maxLayer(cell);
        positions{s}(i) = (transitions{s}.layer(i) - minLayer) / (maxLayer - minLayer);
    end
    mean_position{s} = mean(positions{s});
    CI_position = 1.96 * std(positions{s}) / sqrt(length(positions{s}));
    [h(:,s), ~] = histcounts(positions{s}, edges);
    h(:,s) = h(:,s) ./ numel(positions{s});
end
b = bar(x+0.015*ones(size(x))*[-1 1], h, 'BarWidth', 6);
for s = 1:length(shapes)
    set(b(s), 'FaceColor', colors(s,:), 'EdgeColor', 'none')
    xline(mean_position{s}, '--', 'LineWidth', 2*LineWidth, 'Color', colors(s,:));
    text(0.7, 0.18-(s-1)/90, ['T' shapes{s}(2:end)], 'FontSize', FontSize, 'Color', colors(s,:))
    text(0.8, 0.18-(s-1)/90, [':  N = ' num2str(nCells{s}) ' cells'], 'FontSize', FontSize, 'Color', colors(s,:))
end

xlim([0 1])
ylim([0 0.18])
box('off')
set(gca, 'FontSize', FontSize, 'LineWidth', LineWidth, 'TickDir', 'out')
ax = gca;
ax.XAxis.MinorTick = 'on';
ax.XAxis.MinorTickValues = 0.1:0.2:0.9;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 0.01:0.02:0.17;
xlabel('apical                          relative position                          basal')
ylabel('fraction of T1 transitions')
xtickformat('%.1f')
ytickformat('%.2f')
yline(0.1, 'LineWidth', 2*LineWidth);
text(0.85, 0.107, 'uniform', 'FontSize', FontSize)

% print(gcf, 'apical-basal_position.pdf', '-dpdf', ['-r' num2str(resolution)]);
% print(gcf, 'apical-basal_position.png', '-dpng', ['-r' num2str(resolution)]);
