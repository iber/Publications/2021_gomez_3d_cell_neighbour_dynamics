% overall figure size
aspectRatio = 4/3;
screenHeight = 600; % height in pixels for the window
vectorHeight = 8; % height in inches for exporting
resolution = 300; % DPI for exporting (higher -> larger image)

FontSize = 22;
LineWidth = 2;

e = @(n) (1+2*cos(2*pi./n)) ./ (2*sin(2*pi./n));
dedn = @(n) pi * (2+cos(2*pi./n)) ./ (n.^2.*sin(2*pi./n).^2);
ratio_analytical = @(n) (1-dedn(n)./(n.*e(n))).^(-pi/2);

ell = readtable('ellipticity_tube.csv');
cellLayers = readtable('layers_tube.csv');
transitions = readtable('location_tube.csv');
nCells = max(transitions.cell);
nnc = zeros(nCells,1);
for i = 1:nCells
    nnc(i) = sum(abs(transitions.type(transitions.cell == i)));
end

a_inner = mean(ell.a_inner);
a_outer = mean(ell.a_outer);
b_inner = mean(ell.b_inner);
b_outer = mean(ell.b_outer);

R_min_inner = b_inner.^2 ./ a_inner;
R_max_inner = a_inner.^2 ./ b_inner;
R_min_outer = b_outer.^2 ./ a_outer;
R_max_outer = a_outer.^2 ./ b_outer;

ratio_max = R_max_outer / R_max_inner
ratio_min = R_min_outer / R_min_inner

n = [];
R2_R1_ratio = [];
rel_pos = [];
for i = 1:nCells
    % obtain all T1 transitions in this cell
    idx = (transitions.cell == i);
    layers = transitions.layer(idx);
    neighbours = transitions.n(idx);
    types = transitions.type(idx);
    
    % make sure they are sorted from apical to basal
    [layers, idx] = sort(layers);
    neighbours = neighbours(idx);
    types = types(idx);
    
    % determine the apical-basal position of the transitions
    minLayer = cellLayers.minLayer(i);
    maxLayer = cellLayers.maxLayer(i);
    positions = (layers - minLayer) / (maxLayer - minLayer);
    
    % determine the change of curvature between transitions
    a = a_inner + positions * (a_outer - a_inner);
    b = b_inner + positions * (b_outer - b_inner);
    R = a.^2 ./ b; % radius of curvature on the minor vertex of an ellipse
    ratios = R(2:end) ./ R(1:end-1);
    
    % store n, the change of curvature, and the apical-basal position
    n = [n; neighbours(1:end-1)];
    R2_R1_ratio = [R2_R1_ratio; ratios];
    rel_pos = [rel_pos; positions];
end

% make sure n is sorted for correct visualization with boxplot
[n, idx] = sort(n);
R2_R1_ratio = R2_R1_ratio(idx);
rel_pos = rel_pos(idx);

% global statistics
mean_ratio = mean(R2_R1_ratio)
CI_ratio = 1.96 * std(R2_R1_ratio) / sqrt(length(R2_R1_ratio))

figure
set(gcf, 'Position', [0 0 screenHeight*[aspectRatio 1]], 'Color', 'white')
set(gcf, 'PaperSize', vectorHeight*[aspectRatio 1]) % for PDF and postscript export
set(gcf, 'PaperPosition', [0 0 get(gcf, 'PaperSize')]) % for image export
hold on

scatterWidth = 0.333;
dotalpha = 0.5;
dotarea = 50;
scatter(n+scatterWidth*(rand(size(n))-0.5), R2_R1_ratio, dotarea*ones(size(n)), rel_pos, 'o', 'filled', 'MarkerEdgeColor', 'black', 'MarkerFaceAlpha', dotalpha, 'LineWidth', 0.2)
caxis([0 1])
p = get(gca, 'Position'); % position and size of the axes relative to the full figure: [x y width height]
cb = colorbar('Position', [p(1)+0.85*p(3) p(2)+0.5*p(4) 0.03*p(3), 0.25*p(4)]); % [x y width height] relative to full figure
set(cb, 'YAxisLocation', 'right', 'FontSize', FontSize, 'Ticks', [0 1], 'TickLabels', {'apical', 'basal'}, 'Box', 'off')

nplotrange = [min(n)-0.5 max(n)+0.5];
yline(mean_ratio, 'r-', 'LineWidth', LineWidth);
fill([nplotrange fliplr(nplotrange)], mean_ratio+[-CI_ratio -CI_ratio CI_ratio CI_ratio], 'r', 'EdgeColor', 'none', 'FaceAlpha', 0.1, 'HandleVisibility', 'off')

nfine = 3.64:0.01:10.5;
plot(nfine, ratio_analytical(nfine), 'b')

boxplot(R2_R1_ratio, n, 'Positions', unique(n), 'Symbol', '', 'Colors', 'k')
set(findobj(gca, 'type', 'line'), 'LineWidth', LineWidth) % set linewidth of boxes
set(findobj(gcf, 'LineStyle', '--'), 'LineStyle', '-') % change dashed whiskers to solid lines

xticks(min(n):max(n))
ytickformat('%.1f')
xlim(nplotrange)
ylim([0.95 2])
set(gca, 'FontSize', FontSize, 'LineWidth', LineWidth)
box('off')
xlh = xlabel('neighbour number{\it n}', 'Interpreter', 'tex');
ylabel('curvature change between T1 transitions, {\it{R}}_2/{\it{R}}_1', 'Interpreter', 'tex')
legend({'Observed data', ['Overall mean ' num2str(round(mean_ratio,3)) '±' num2str(round(CI_ratio,3)) ' (95% CI)'], 'Curvature theory'}, 'FontSize', FontSize)
legend boxoff

xlh.Position(2) = xlh.Position(2) - range(ylim)/100;

ax = gca;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 0.05:0.1:0.55;

% draw polygons
set(gca, 'Clipping', 'off') % enable drawing outside of axes
r = 0.22; % radius
polygonX = @(n) -r*sin(2*pi*(linspace(0,1,n+1)+mod(n+1,2)/(2*n)));
polygonY = @(n)  r*cos(2*pi*(linspace(0,1,n+1)+mod(n+1,2)/(2*n)));
p = get(gca, 'Position'); % position and size of the axes relative to the full figure: [x y width height]
axisAspectRatio = range(ylim)/range(xlim) * p(3)/p(4) * aspectRatio;
for i = 3:10
    plot(polygonX(i) + i, polygonY(i)*axisAspectRatio + min(ylim) - range(ylim)/25, 'k-', 'LineWidth', LineWidth, 'HandleVisibility', 'off')
end

% print(gcf, 'curvature_change_distribution.pdf', '-dpdf', ['-r' num2str(resolution)]);
% print(gcf, 'curvature_change_distribution.png', '-dpng', ['-r' num2str(resolution)]);
