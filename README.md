# 2021_Gomez_3D_Cell_Neighbour_Dynamics

H. Gómez, M. Dumond, L. Hodel, R. Vetter, D. Iber, *3D Cell Neighbour Dynamics in Growing Pseudostratified Epithelia*

A preprint of the article can be found on bioRxiv at: https://doi.org/10.1101/2021.03.05.434123

## Contents

* figure3
  * number_of_transitions.m - MATLAB script that produces Figures 3d & e
  * curvature_T1.m - MATLAB script that produces Figure 3g
  * ellipticity_tube.csv - Comma-separated table of fitted ellipse semi-axes of the tubular epithelium
  * layers_tip.csv - Comma-separated table of cell end points along the apicobasal axis in the tip
  * layers_tube.csv - Comma-separated table of cell end points along the apicobasal axis in the tube
  * location_tip.csv - Comma-separated table of the locations of T1 transitions in the tip
  * location_tube.csv - Comma-separated table of the locations of T1 transitions in the tube
* figure5
  * T1_nucleus_correlation.m - MATLAB script that produces Figure 5j
  * cell_properties.csv - Comma-separated table of cell properties along their apicobasal axis
* figure_data
  * figure2_data1.csv - Comma-separated table with 2.5D neighbour number and cell area
  * figure2_data2.csv - Comma-separated table with 2.5D Aboav-Weaire data
  * figure3_data1.csv - Comma-separated table with 3D neighbour number and cell area
  * figure3_data2.csv - Comma-separated table with 3D tissue curvature fold-change
  * figure4_data1.csv - Comma-separated table with 3D area CV and average neighbour number
  * figure4_data2.csv - Comma-separated table with 3D Aboav-Weaire data
  * figure4_data3.csv - Comma-separated table with 3D neighbour number and exchange type
  * figure5_data1.csv - Comma-separated table with 3D neighbour number, cell area, and nucleus area
  * figure5_data2.csv - Comma-separated table with nucleus volume, nucleus ellipticity, cell diameter, nucleus diameter and center of mass position
  * figure6_data1.csv - Comma-separated table with 2.5D neighbour number and cell area over time
  * figure6_data2.csv - Comma-separated table with 2.5D area CV over time
  * figure6_data3.csv - Comma-separated table with 2.5D Aboav-Weaire data over time
  * figure7_data1.csv - Comma-separated table with 3D neighbour number and cell area over time
  * figure7_data2.csv - Comma-separated table with 3D area CV and hexagon percentage over time
  * figure7_data3.csv - Comma-separated table with 3D Aboav-Weaire data over time
* LICENSE - License file.

## Use of data in figures

* Figure 2: figure2_data1.csv, figure2_data2.csv
* Figure 3: figure3_data1.csv, figure3_data2.csv
* Figure 4: figure3_data1.csv, figure4_data1.csv, figure4_data2.csv, figure4_data3.csv
* Figure 5: figure5_data1.csv, figure5_data2.csv
* Figure 6: figure6_data1.csv, figure6_data2.csv, figure6_data3.csv
* Figure 7: figure7_data1.csv, figure7_data2.csv, figure7_data3.csv
* Figure 8: figure7_data1.csv

## License

All source code and data in this repository is released under the 3-Clause BSD license (see LICENSE for details).
