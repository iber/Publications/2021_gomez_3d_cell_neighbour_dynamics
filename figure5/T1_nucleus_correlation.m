FontSize = 18;
LineWidth = 1;

T = readtable('cell_properties.csv');
nCells = max(T.cell);

pos_inc = [];
pos_dec = [];
for i = 1:nCells
    idx = find(T.cell == i);
    nucleusStart = find(T.nucleus_area_um2(idx)~=0, 1, 'first') - 0.5;
    nucleusEnd   = find(T.nucleus_area_um2(idx)~=0, 1, 'last' ) + 0.5;
    transitions = diff(T.n(idx));
    inc = find(transitions > 0) + 0.5;
    dec = find(transitions < 0) + 0.5;
    pos_inc = [pos_inc; (inc - nucleusStart) / (nucleusEnd - nucleusStart)];
    pos_dec = [pos_dec; (dec - nucleusStart) / (nucleusEnd - nucleusStart)];
end

edges = -2:0.2:3;

close all
histogram(pos_inc, edges, 'Normalization', 'probability', 'LineWidth', LineWidth, 'DisplayName', 'Neighbour increase')
hold all
histogram(pos_dec, edges, 'Normalization', 'probability', 'LineWidth', LineWidth, 'DisplayName', 'Neighbour decrease')
xline(0, 'k--', 'LineWidth', 3*LineWidth, 'HandleVisibility', 'off');
xline(1, 'k--', 'LineWidth', 3*LineWidth, 'HandleVisibility', 'off');
xlim([min(edges) max(edges)])
ylim([0 0.25])
xticks([-2, -1, 0, 1, 2, 3])
xticklabels(["-200%", "-100%", "begin", "end", "+100%", "+200%"])
set(gca, 'FontSize', FontSize, 'LineWidth', LineWidth)
xlabel('apicobasal position relative to nucleus', 'FontSize', FontSize)
ylabel('frequency of T1 transitions', 'FontSize', FontSize)
legend('Location', 'northwest')
ytickformat('%.2f')
pos = get(gca, 'Position');
Start = [0, 0.18];
End   = [1, 0.18];
annotation('doublearrow', ...
    [(End(1) + abs(min(xlim)))/diff(xlim) * pos(3) + pos(1), ...
     (Start(1) + abs(min(xlim)))/diff(xlim) * pos(3) + pos(1) ], ... 
    [(End(2) - min(ylim))/diff(ylim) * pos(4) + pos(2), ...
     (Start(2) - min(ylim))/diff(ylim) * pos(4) + pos(2)], ...
    'LineWidth', LineWidth);
Start = [-1.8, 0.08];
End   = [-1, 0.08];
annotation('arrow', ...
    [(End(1) + abs(min(xlim)))/diff(xlim) * pos(3) + pos(1), ...
     (Start(1) + abs(min(xlim)))/diff(xlim) * pos(3) + pos(1) ], ... 
    [(End(2) - min(ylim))/diff(ylim) * pos(4) + pos(2), ...
     (Start(2) - min(ylim))/diff(ylim) * pos(4) + pos(2)], ...
    'LineWidth', LineWidth);
Start = [2.8, 0.08];
End   = [2, 0.08];
annotation('arrow', ...
    [(End(1) + abs(min(xlim)))/diff(xlim) * pos(3) + pos(1), ...
     (Start(1) + abs(min(xlim)))/diff(xlim) * pos(3) + pos(1) ], ... 
    [(End(2) - min(ylim))/diff(ylim) * pos(4) + pos(2), ...
     (Start(2) - min(ylim))/diff(ylim) * pos(4) + pos(2)], ...
    'LineWidth', LineWidth);
text(0.5, 0.19, 'Nucleus', 'HorizontalAlignment', 'center', 'FontSize', FontSize)
text(-1.4, 0.09, 'apical', 'HorizontalAlignment', 'center', 'FontSize', FontSize)
text(2.4, 0.09, 'basal', 'HorizontalAlignment', 'center', 'FontSize', FontSize)
set(gca,'OuterPosition',[0 0.01 1 0.99])

% saveas(gcf, 'T1_nucleus_correlation.pdf')
